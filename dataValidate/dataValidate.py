import pandas_schema
from pandas_schema import Column
from validate.FuncValidator import *
from loguru import logger
import pandas as pd
import sys


class DataValidate:
    def __init__(self, listOfFields, json_schema):
        self.listOfFields = listOfFields
        self.json_schema = json_schema

    def validateSchema(self):
        column_list = [Column(k, [get_validator(v) for v in vals]) for k, vals in self.json_schema['validate'].items()]

        schema = pandas_schema.Schema(column_list)

        errors = schema.validate(self.listOfFields)

        errors_index_rows = [e.row for e in errors]

        errorList = [" "] * len(self.listOfFields.index)

        for err in errors:
            errorList[err.row] += str(err.column) + " " + str(err.message)

        self.listOfFields.loc[:, "Error"] = errorList

        try:
            baseOK = self.listOfFields.drop(index=errors_index_rows)
        except Exception as error:
            diff = get_different_column(list(self.listOfFields.columns), self.json_schema['validate'])
            print(f'Errors: {errors[0]}')
            print(f'Columns: {diff}') if diff else None
            sys.exit()

        baseNOK = self.listOfFields.iloc[list(
            set(self.listOfFields.index) - set(baseOK.index))]

        keep = self.json_schema['keepDuplicity']
        sort_by = self.json_schema['sortBy'] if 'sortBy' in self.json_schema else ''
        baseOK, duplicateData = _dropDuplicatedData(baseOK, keep, sort_by=sort_by, hashID='hashID')

        if not duplicateData.empty:
            duplicateData.loc[:, 'Error'] = 'Duplicate data'

        baseNOK = pd.concat([baseNOK, duplicateData])

        if 'Error' in baseOK:
            baseOK.drop('Error', axis=1, inplace=True)

        return baseOK, baseNOK


def _dropDuplicatedData(dataframe, keep, sort_by=None, hashID="hashID"):
    dataframe.sort_values(by=[sort_by], inplace=True) if sort_by else pd.DataFrame()
    uniques = dataframe.drop_duplicates(subset=[hashID], keep=keep)
    duplicates = dataframe[dataframe.duplicated(subset=[hashID], keep=keep)]
    return uniques, duplicates


def get_different_column(df_columns, schema_validate):
    try:
        df_columns = list(filter(lambda x: x != 'Error', df_columns))
        validate_columns = []
        for key, value in schema_validate.items():
            validate_columns.append(key)

        if len(df_columns) > len(validate_columns):
            biggest_list = df_columns
            shortest_list = validate_columns
        else:
            biggest_list = validate_columns
            shortest_list = df_columns

        diff = [biggest for biggest in biggest_list if biggest not in shortest_list]
        return diff
    except:
        return ''
