import pandas as pd


def read_csv():
    df = pd.read_csv('../data/util/template_schema.csv', sep=",")
    return df


def print_dict(dictionary, name):
    final_text = f'"{name}": {{\n'

    for key in dictionary:
        final_text += f'    "{key}" : {dictionary[key]},\n'

    final_text += '    "hashID": [["notNull"]]\n'
    final_text += '}\n'

    final_text = final_text.replace("'", '"')
    final_text = final_text.replace(" [None],", '')
    final_text = final_text.replace(" [None]", '')
    final_text = final_text.replace("],]", ']]')
    print(final_text)


def handle_validate_schema(type_data, size_text, size_text_mandatory, mandatory):
    def handle_size_text(size):
        numbers = {
            1: 'One',
            2: 'Two',
            3: 'Three',
            4: 'Four',
            6: 'Six',
            7: 'Seven',
            8: 'Eight',
            9: 'Nine',
            11: 'Eleven',
            14: 'Fourteen',
            44: 'Forty-four',
            60: 'Sixty'
        }
        text = None
        if size_text_mandatory == 'yes':
            size = numbers[size]
            text = f'lenEqual{size}'
        else:
            try:
                text = f'lenMax{int(size)}'
            except ValueError:
                text = None
        return text

    def handle_mandatory(mand):
        if mand == 'yes':
            return "notNull"

    size_text = handle_size_text(size_text)
    mandatory = handle_mandatory(mandatory)

    return [[type_data], [size_text], [mandatory]]


if __name__ == '__main__':
    dataframe = read_csv()
    print(dataframe['FIELD'].tolist())
    validate = {}
    for index, row in dataframe.iterrows():
        field = row['FIELD']
        validate[field.strip()] = handle_validate_schema(row['TYPE'],
                                                         row['SIZE_TEXT'],
                                                         row['SIZE_TEXT_MANDATORY'],
                                                         row['MANDATORY'])

    print_dict(validate, 'validate')
