import pandas as pd
import numpy as np


class ReadExcel:
    def __init__(self, path):
        self.__path = path

    def readExcel(self, sheet):
        xlsx = pd.ExcelFile(self.__path)
        dataframe = pd.read_excel(xlsx, sheet, dtype=str)
        dataframe[dataframe.columns] = dataframe.apply(lambda item: item.str.strip())
        dataframe.replace(np.nan, '', regex=True, inplace=True)
        return dataframe

    def readCsv(self):
        dataframe = pd.read_csv(self.__path, sep=";", dtype=str)
        dataframe[dataframe.columns] = dataframe.apply(lambda item: item.str.strip())
        dataframe.replace(np.nan, '', regex=True, inplace=True)
        return dataframe
