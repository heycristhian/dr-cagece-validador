validate = {
       "periodo" : [["isDate"], ["notNull"]],
       "M200_REG" : [["isString"], ["lenMax4"]],
       "M210_REG" : [["isString"], ["lenEqualFour"], ["notNull"]],
       "M210_COD_CONT" : [["isString"], ["lenEqualTwo"], ["notNull"]],
       "M210_VL_REC_BRT" : [["isDecimal"], ["notNull"]],
       "M210_VL_BC_CONT" : [["isDecimal"], ["notNull"]],
       "M210_VL_AJUS_ACRES_BC_PIS" : [["isDecimal"], ["notNull"]],
       "M210_VL_AJUS_REDUC_BC_PIS" : [["isDecimal"], ["notNull"]],
       "M210_VL_BC_CONT_AJUS" : [["isDecimal"], ["notNull"]],
       "M210_ALIQ_PIS" : [["isDecimal"]],
       "M210_QUANT_BC_PIS" : [["isDecimal"]],
       "M210_ALIQ_PIS_QUANT" : [["isDecimal"]],
       "M210_VL_CONT_APUR" : [["isDecimal"], ["notNull"]],
       "M210_VL_AJUS_ACRES" : [["isDecimal"], ["notNull"]],
       "M210_VL_AJUS_REDUC" : [["isDecimal"], ["notNull"]],
       "M210_VL_CONT_DIFER" : [["isDecimal"]],
       "M210_VL_CONT_DIFER_ANT" : [["isDecimal"]],
       "M210_VL_CONT_PER" : [["isDecimal"], ["notNull"]],
       "M215_REG" : [["isString"], ["lenMax4"], ["notNull"]],
       "M215_IND_AJ_BC" : [["isString"], ["lenEqualOne"], ["notNull"]],
       "M215_VL_AJ_BC" : [["isDecimal"], ["notNull"]],
       "M215_COD_AJ_BC" : [["isString"], ["lenEqualTwo"], ["notNull"]],
       "M215_NUM_DOC" : [["isString"], ["lenMax255"]],
       "M215_DESCR_AJ_BC" : [["isString"], ["lenMax255"]],
       "M215_DT_REF" : [["isString"], ["lenEqualEight"]],
       "M215_COD_CTA" : [["isString"], ["lenMax255"]],
       "M215_CNPJ" : [["isString"], ["lenEqualFourteen"], ["notNull"]],
       "M215_INFO_COMPL" : [["isString"], ["lenMax255"]],
       "hashID": [["notNull"]]
   }

fields = []
for v in validate:
    fields.append(v)

fields.remove('hashID')
print(fields)
