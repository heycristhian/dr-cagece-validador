import json


bloco = 'blocoF'
schema = 'Registro_F600'

with open(f'../../data/schemas/{bloco}/{schema}.json') as file:
    j = json.load(file)

list_decimal = []

for validate in j['validate']:
    if 'isDecimal' in str(j['validate'][validate]):
        list_decimal.append(validate)

print(f"change_type_column(dataframe, {list_decimal}, float)")