from datetime import datetime

import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
import hashlib
import sys
from loguru import logger


def apply_rules(schema, dataframe):
    logger.info('Applying the rules')
    for rule in schema['rules']:
        dataframe = eval(rule)
    return dataframe


def plus_datetime(date_time, days=None, months=None, years=None):
    new_datetime = date_time
    new_datetime = new_datetime + relativedelta(days=days) if days else new_datetime
    new_datetime = new_datetime + relativedelta(months=months) if months else new_datetime
    new_datetime = new_datetime + relativedelta(years=years) if years else new_datetime
    new_datetime = new_datetime.replace(hour=0, minute=0, second=0, microsecond=0)
    return new_datetime


def createHash(dataframe, listofColumns, colname="hashID"):
    hash = lambda strval: hashlib.md5(strval.encode('utf-8)')).hexdigest()
    try:
        dataframe[colname] = dataframe[listofColumns].apply(lambda row: '_'.join(row.values.astype(str)), axis=1)
        dataframe[colname] = dataframe[colname].apply(hash)
        return dataframe
    except KeyError as err:
        raise Exception(f'Coluna especificada não existe no dataframe atual para geração do hash! \nColumn: {err}')


def rename_columns(dataframe: pd.DataFrame, from_to_columns=None, change_all_columns=False):
    """
    date: 07/04/2021
    author: Cristhian Dias
    :param change_all_columns: Todas colunas do dataframe vão ser renomeadas para lower case
    :param dataframe: Dataframe que será alterado o nome da coluna
    :param from_to_columns: Dicionário, onde chave é o nome da coluna do dataframe atual e o valor será o novo nome
    :return: Retorna um novo dataframe com as colunas renomeadas
    """
    if not from_to_columns:
        from_to_columns = {}

    columns = []
    if change_all_columns:
        columns.extend(list(dataframe.columns))

    for column in columns:
        if column not in from_to_columns.keys():
            from_to_columns[column] = column.lower()
    dataframe.rename(columns=from_to_columns, inplace=True)
    return dataframe


def remove_columns(dataframe: pd.DataFrame, columns):
    for column in columns:
        if column in dataframe:
            dataframe.drop(column, axis=1, inplace=True)
    return dataframe


def change_type_column(dataframe: pd.DataFrame, columns, type_column):
    last_column = None
    for column in columns:
        if type_column == float:
            dataframe[column] = dataframe[column].map(lambda item: '0' if not item else item)
            dataframe[column] = dataframe[column].map(lambda item: item.replace(',', '.'))
        try:
            last_column = column
            if type_column == datetime:
                dataframe[column] = pd.to_datetime(dataframe[column])
            else:
                dataframe[column] = dataframe[column].astype(type_column)
        except ValueError as err:
            logger.error(f'Incorrect data in column "{last_column}": {err}')
    return dataframe


def date_format(dataframe: pd.DataFrame, column, format_final):
    try:
        datetime_format = '%Y-%m-%d %H:%M:%S'

        def get_date(date):
            size_date = 0
            new_date_str = None
            date = str(date)
            new_date = datetime.strptime(date, datetime_format)
            if format_final == 'MMYYYY':
                size_date = 6
                new_date_str = f'0{new_date.date().month}{new_date.date().year}'
            elif format_final == 'DDMMYYYY':
                size_date = 8
                new_date_str = f'0{new_date.date().month}{new_date.date().year}'
                new_date_str = new_date_str[-6:]
                new_date_str = f'0{new_date.date().day}{new_date_str}'
            return new_date_str[-size_date:]

        dataframe[column] = dataframe[column].map(lambda date: get_date(date))
        return dataframe
    except (ValueError, TypeError) as err:
        logger.error(f'There was a problem with date_format: {err}')
        return dataframe


def new_column(dataframe: pd.DataFrame, column, value):
    dataframe[column] = value
    return dataframe


def remove_characters(dataframe: pd.DataFrame, columns, characters):
    def remove(value: str):
        for character in characters:
            value = str(value)
            value = value.replace(character, '')
        return value

    if isinstance(columns, list):
        for column in columns:
            dataframe[column] = dataframe[column].map(lambda item: remove(item))
        return dataframe
    dataframe[columns] = dataframe[columns].map(lambda item: remove(item))
    return dataframe


def change_value(dataframe, column, current_value, new_value):

    try:
        dataframe[column] = dataframe[column].map(lambda item: new_value if item.strip() == current_value else item)
    except AttributeError as err:
        dataframe[column] = dataframe[column].map(lambda item: new_value if item == current_value else item)
    return dataframe


def text_to_date(dataframe, column, format_date):
    """
    :param dataframe:
    :param column:
    :param format_date: %Y%m / %Y-%m / %Y%m%d
    :return:
    """
    def to_date(date):
        try:
            return datetime.strptime(date, format_date)
        except ValueError as err:
            return date
    dataframe[column] = dataframe[column].astype(str)
    dataframe[column] = dataframe[column].map(lambda date: to_date(date))
    return dataframe


def add_zero_left(dataframe, column, size):
    def add_zero(value):
        if value == '':
            return value
        elif len(value) <= size:
            value = f'000000{value}'
            return value[-size:]
        else:
            value = value[1:] if value[0] == '0' else value
        return value

    dataframe[column] = dataframe[column].map(lambda item: add_zero(str(item)))
    return dataframe


def change_first_day(dataframe: pd.DataFrame, column):
    dataframe[column] = dataframe[column].map(lambda item: plus_datetime(item, days=-item.day+1))


def get_diff_values(values_of_columns, type_value, dictionary):
    try:
        not_in = {value for value in values_of_columns if value.lower() not in dictionary}
        dataframe = pd.DataFrame({
            'key': list(not_in),
            'type': type_value
        })
        return dataframe
    except Exception as err:
        print(f'Error (get_diff_values): {err}')
        sys.exit()
