import re
import dateutil.parser
from decimal import *

from pandas_schema.validation import (LeadingWhitespaceValidation, TrailingWhitespaceValidation,
                                      CustomElementValidation,
                                      IsDistinctValidation)


def check_decimal(dec):
    if dec == '':
        return True
    if isinstance(dec, bool):
        return False
    if isinstance(dec, float) or isinstance(dec, int):
        return True
    return False


def check_int(num):
    if num == '':
        return True
    if isinstance(num, int):
        return True
    return False


def check_date(date):
    if str(date) == '':
        return True
    try:
        dateutil.parser.parse(str(date))
        return True
    except Exception:
        return False


def check_cnpj(cnpj):
    cnpj = ''.join(re.findall('\d', str(cnpj)))

    if (not cnpj) or (len(cnpj) < 14):
        return False

    # Pega apenas os 12 primeiros dígitos do CNPJ e gera os 2 dígitos que faltam
    inteiros = list(map(int, cnpj))
    novo = inteiros[:12]

    prod = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
    while len(novo) < 14:
        r = sum([x * y for (x, y) in zip(novo, prod)]) % 11
        if r > 1:
            f = 11 - r
        else:
            f = 0
        novo.append(f)
        prod.insert(0, 6)

    # Se o número gerado coincidir com o número original, é válido
    if novo == inteiros:
        return True
    return False


def check_gtz(number):
    try:
        number = float(number)
    except ValueError:
        return True
    check = number > 0
    return check


def check_only_numbers(item):
    if item == '':
        return True
    try:
        [print(number) for number in item]
        return True
    except TypeError:
        return False
    
    
def len_max(value, comparator_number):
    return len(str(value)) <= comparator_number or str(value) == ''


def len_equal(value, comparator_number):
    return len(str(value)) == comparator_number or str(value) == ''


def get_validator(opts):
    VALIDATORS = {
        'isDecimal': (CustomElementValidation, [lambda d: check_decimal(d), 'is not decimal;']),
        'isInt': (CustomElementValidation, [lambda i: check_int(i), 'is not integer;']),
        'notNull': (CustomElementValidation, [lambda d: str(d) != '', 'this field cannot be null;']),
        'isCNPJ': (CustomElementValidation, [lambda cnpj: check_cnpj(cnpj), 'invalid;']),
        'isDate': (CustomElementValidation, [lambda date: check_date(date), 'this field does not have a valid date;']),
        'none': (CustomElementValidation, [lambda t: len(str(t)) >= 0, '']),
        'lenEqualOne': (CustomElementValidation, [lambda t: len_equal(t, 1), 'must be equal to 1;']),
        'lenEqualTwo': (CustomElementValidation, [lambda t: len_equal(t, 2), 'must be equal to 2;']),
        'lenEqualThree': (CustomElementValidation, [lambda t: len_equal(t, 3), 'must be equal to 3;']),
        'lenEqualFour': (CustomElementValidation, [lambda t: len_equal(t, 4), 'must be equal to 4;']),
        'lenEqualSix': (CustomElementValidation, [lambda t: len_equal(t, 6), 'must be equal to 6;']),
        'lenEqualSeven': (CustomElementValidation, [lambda t: len_equal(t, 7), 'must be equal to 7;']),
        'lenEqualEight': (CustomElementValidation, [lambda t: len_equal(t, 8), 'must be equal to 8;']),
        'lenEqualNine': (CustomElementValidation, [lambda t: len_equal(t, 9), 'must be equal to 9;']),
        'lenEqualEleven': (CustomElementValidation, [lambda t: len_equal(t, 11), 'must be equal to 11;']),
        'lenEqualFourteen': (CustomElementValidation, [lambda t: len_equal(t, 14), 'must be equal to 14;']),
        'lenEqualFortyOne': (CustomElementValidation, [lambda t: len_equal(t, 41), 'must be equal to 41;']),
        'lenEqualFortyFour': (CustomElementValidation, [lambda t: len_equal(t, 44), 'must be equal to 44;']),
        'lenEqualSixty': (CustomElementValidation, [lambda t: len_equal(t, 60), 'must be equal to 60;']),
        'lenMax2': (CustomElementValidation, [lambda t: len_max(t, 2), 'must be a maximum of 2 characters;']),
        'lenMax3': (CustomElementValidation, [lambda t: len_max(t, 3), 'must be a maximum of 3 characters;']),
        'lenMax4': (CustomElementValidation, [lambda t: len_max(t, 4), 'must be a maximum of 4 characters;']),
        'lenMax5': (CustomElementValidation, [lambda t: len_max(t, 5), 'must be a maximum of 5 characters;']),
        'lenMax6': (CustomElementValidation, [lambda t: len_max(t, 6), 'must be a maximum of 6 characters;']),
        'lenMax8': (CustomElementValidation, [lambda t: len_max(t, 8), 'must be a maximum of 8 characters;']),
        'lenMax9': (CustomElementValidation, [lambda t: len_max(t, 9), 'must be a maximum of 9 characters;']),
        'lenMax10': (CustomElementValidation, [lambda t: len_max(t, 10), 'must be a maximum of 10 characters;']),
        'lenMax11': (CustomElementValidation, [lambda t: len_max(t, 11), 'must be a maximum of 11 characters;']),
        'lenMax14': (CustomElementValidation, [lambda t: len_max(t, 14), 'must be a maximum of 14 characters;']),
        'lenMax15': (CustomElementValidation, [lambda t: len_max(t, 15), 'must be a maximum of 15 characters;']),
        'lenMax20': (CustomElementValidation, [lambda t: len_max(t, 20), 'must be a maximum of 20 characters;']),
        'lenMax60': (CustomElementValidation, [lambda t: len_max(t, 60), 'must be a maximum of 60 characters;']),
        'lenMax100': (CustomElementValidation, [lambda t: len_max(t, 100), 'must be a maximum of 100 characters;']),
        'lenMax255': (CustomElementValidation, [lambda t: len_max(t, 255), 'must be a maximum of 255 characters;']),
        'isString': (CustomElementValidation, [lambda item: isinstance(item, str) or item == '', 'must be String;']),
        'onlyNumbers': (CustomElementValidation, [lambda item: check_only_numbers(item), 'must have only numbers;']),
        'isOthers': (TrailingWhitespaceValidation, []),
        'isUnique': (IsDistinctValidation, []),
        'greaterThanZero': (CustomElementValidation, [
            lambda number: check_gtz(number), 'this number is less than or equal to zero '])
    }
    func, args = VALIDATORS[opts[0]]
    args.extend(opts[1:])
    return func(*args)
