import json

from loguru import logger
from pymongo import MongoClient

from dataValidate.dataValidate import DataValidate
from util.ReadExcel import ReadExcel
from validate.Rules import createHash, apply_rules


def process(bloco):
    path = bloco['path']
    sheets = bloco['sheets']
    bloco_name = bloco['name']
    isCsv = 'type' in bloco and bloco['type'] == 'csv'
    dataframes = {}
    reader = ReadExcel(path)

    logger.info('Read excel')
    drop_database('cagece')

    if isCsv:
        for sheet in sheets:
            dataframes[sheet] = reader.readCsv()
    else:
        for sheet in sheets:
            dataframes[sheet] = reader.readExcel(sheet)

    for key in dataframes:
        logger.warning(f'Processando {bloco_name} no sheet {key}')
        schema = load_json(f'schemas/{bloco_name}', key)

        if 'rules' in schema:
            apply_rules(schema, dataframes[key])

        dataframes[key] = createHash(dataframes[key], schema['comparatorFields'])
        print(f'Worksheet current: {key}')
        baseOK, baseNOK = validation(dataframes[key], schema)
        save(baseOK, baseNOK, bloco_name, key)


def validation(df, schema):
    logger.info('Execução do Validador (baseOK, baseNOK)')
    dataValidate = DataValidate(df, schema)
    baseOK, baseNOK = dataValidate.validateSchema()
    return baseOK, baseNOK


def load_json(folder, file_name):
    with open(f'./data/{folder}/{file_name}.json', 'r') as file_json:
        file = json.load(file_json)
        return file


def save(baseOK, baseNOK, bloco_name, sheet_name):
    print(f'Base OK ({bloco_name}: {sheet_name}): {baseOK.shape}')
    print(f'Base NOK ({bloco_name}: {sheet_name}): {baseNOK.shape}')
    insert_mongo(baseOK, 'cagece', f'{bloco_name}_{sheet_name}_OK')
    insert_mongo(baseNOK, 'cagece', f'{bloco_name}_{sheet_name}_NOK')


def drop_database(database):
    client = MongoClient('localhost', 27017)
    client.drop_database(database)


def insert_mongo(dataframe, database, collection_name):
    if dataframe.empty:
        return
    dataframe.fillna(value='', inplace=True)
    data = dataframe.to_dict(orient='records')
    client = MongoClient('localhost', 27017)

    banco = client[database]
    collection = banco[collection_name]
    collection.drop()
    collection.insert_many(data)


if __name__ == '__main__':
    path_info = 'data/info/filesInfo.json'

    with open(path_info, 'r') as info_file:
        files = json.load(info_file)
    process(files['blocoA'])
